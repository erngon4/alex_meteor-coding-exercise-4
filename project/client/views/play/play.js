/*****************************************************************************/
/* Play: Event Handlers and Helpersss .js*/
/*****************************************************************************/

Template.Play.events ({

  'click #playerResponseButton': function (event, template) { //This helped me: http://stackoverflow.com/questions/28034512/dynamic-radio-button-creation-using-handlebars-in-meteor
    event.preventDefault ();
    var playerResponseCode = $ (":radio[name=playerResponsesRadio]:checked").val ();
    console.log ('playerResponseCode: ' + playerResponseCode);

    Meteor.call ('/app/processPlay', Session.get ('SCENARIO'), Session.get ('NPCHARACTER'), Session.get ('NPCHARACTER_REQUEST'),Session.get ('PLAYER_RESPONSES'), playerResponseCode, function (err, response) {
      console.log (JSON.stringify (response));
      Session.set ('result', JSON.stringify (response));

      var score = 0;

      if (Meteor.user().profile.score != undefined) {
        score = Meteor.user().profile.score;

      }

      Session.set('USER_SCORE',score);

      if (response.playResult != null) {

        alert (response.playResult);
      }

      // if new scenario, set new scenario
      // if new npCharacterRequest, set new npCharacterRequest
      // newScenario:null,newNPCharacter:null,newNPCharacterRequest:null
      if (response.newNPCharacterRequest != null) {
        Template.Play.setScenario (Session.get ('SCENARIO'), Session.get ('NPCHARACTER'), response.newNPCharacterRequest);
      }

      if (response.randomScenario === true) {

        Template.Play.randomScenario ();
      }

      if (err) {
        alert (err);
      }
    });
  }
});

Template.Play.helpers ({
  scenario: function () {

    var scenario = Session.get ('SCENARIO');
    return scenario;
  },
  npCharacter: function () {

    var character = Session.get ('NPCHARACTER');
    return character;
  },
  npCharacterRequest: function () {

    var request = Session.get ('NPCHARACTER_REQUEST');
    return request;

  },
  playerResponses: function () {

    var responses = Session.get ('PLAYER_RESPONSES');
    return responses;
  },
  playResult: function () {

    return Session.get ('PLAY_RESULT') ;

  },
  userScore: function () {

  return Session.get ('USER_SCORE');

}
});

Template.Play.randomScenario = function () {
  // Random Scenario

  var scenarios = Scenario.find ({}).fetch ();

  var randomIndex =  0;//Math.floor ((Math.random () * scenarios.length));

  if (Meteor.user().profile.counter != undefined) {
    randomIndex = Meteor.user().profile.counter;

  }

  if(randomIndex > scenarios.length)
  {
    randomIndex = 0;
  }

  var randomScenario = scenarios[randomIndex];

  console.log ('randomScenario: ' + JSON.stringify (randomScenario));

  // Random Character
  var npCharacters = NPCharacter.find ({scenarioCodes: randomScenario.objectCode}).fetch ();

  console.log ('npCharacters: ' + JSON.stringify (npCharacters));

  var randomIndex = Math.floor ((Math.random () * npCharacters.length));

  var randomCharacter = npCharacters [randomIndex];

  console.log ('randomCharacter: ' + JSON.stringify (randomCharacter));

  var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

  console.log ('requests: ' + JSON.stringify (requests));

  var randomIndex = Math.floor ((Math.random () * requests.length));

  var randomRequest = requests[randomIndex];

  console.log ('randomRequest: ' + JSON.stringify (randomRequest));

  Template.Play.setScenario (randomScenario, randomCharacter, randomRequest);
}

Template.Play.setScenario = function (scenario, character, request) {

  Session.set ('SCENARIO', scenario);
  Session.set ('NPCHARACTER', character);
  Session.set ('NPCHARACTER_REQUEST', request);

  var playerResponses = PlayerResponse.find ({npCharacterRequestCodes: request.objectCode}).fetch ();

  Session.set ('PLAYER_RESPONSES', playerResponses);

  console.log ('playerResponses: ' + JSON.stringify (playerResponses));
}

/*****************************************************************************/
/* Play: Lifecycle Hooks */
/*****************************************************************************/
Template.Play.created = function () {

  var score = 0;

  if (Meteor.user().profile.score != undefined) {
    score = Meteor.user().profile.score;

  }

  Session.set('USER_SCORE',score);

  Template.Play.randomScenario ();

};

Template.Play.rendered = function () {


};

Template.Play.destroyed = function () {
};